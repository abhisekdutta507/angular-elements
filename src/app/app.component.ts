import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.ShadowDom
})
export class AppComponent {
  public displayText: string = 'Angular Element is rendered properly!';
  public versionText: string = '11.1.0';
}
