import { NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { createCustomElement } from '@angular/elements';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [],
  entryComponents: [AppComponent]
})
export class AppModule {

  /**
   * 
   * @param injector {Injector} instance of the Injector class
   */
  constructor(private injector: Injector) {
      
  }

  ngDoBootstrap() {
    const customElement = createCustomElement(AppComponent, { injector: this.injector });    
    customElements.define('angular-element', customElement);
  }
}
