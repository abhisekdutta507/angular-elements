# Angular Elements

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `npm run build:elements` to build the project. The build artifacts will be stored in the `dist/` directory.

## Install webcomponents

Run the below commands to install the requied dependencies.

```bash
ng add @angular/elements --project=angular-elements

npm i @webcomponents/custom-elements

npm i fs-extra concat -D
```

Make the changes in the `src/polyfills.ts` file.

```ts
import '@webcomponents/custom-elements/src/native-shim';
import '@webcomponents/custom-elements/src/custom-elements';
```

Update the `src/app/app.component.ts` with the below changes.

```ts
import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  // existing parameters

  encapsulation: ViewEncapsulation.ShadowDom
})
```

Specify the app in `src/app/app.module.ts`.

```ts
import { NgModule, Injector } from '@angular/core';
import { createCustomElement } from '@angular/elements';

import { AppComponent } from './app.component';

@NgModule({
  // existing parameters

  entryComponents: [AppComponent]
})
export class AppModule {

  /**
   * 
   * @param injector {Injector} instance of the Injector class
   */
  constructor(private injector: Injector) {
      
  }

  ngDoBootstrap() {
    const customElement = createCustomElement(AppComponent, { injector: this.injector });    
    customElements.define('angular-element', customElement);
  }
}
```

Apply the webcomponent in `src/index.html`.

```html
<angular-element></angular-element>
```

Create the `build-script.js` that will help to merge the build files.

```ts
const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/angular-elements/main.js',
    './dist/angular-elements/polyfills.js',
    './dist/angular-elements/runtime.js'
  ]

  await fs.ensureDir('dist/angular-elements')

  await concat(files, 'dist/angular-elements/script.js')
})()
```

Add the build command in `package.json`.

```json
{
  "scripts": {
    // existing scrips

    "build:elements": "ng build --prod --output-hashing=none && node build-script.js"
  }
}
```

## Output

In the dist folder `script.js` & `styles.css` will be generated.

## Setting up Angular Elements in a Vue.js project

Add the output files in the Vue.js project. Put the files inside the `public` folder.

Update the `public/index.html` with the script & style tags.

```html
<!DOCTYPE html>
<html lang="">
  <head>
    <!-- existing template -->

    <link rel="stylesheet" href="<%= BASE_URL %>styles.css">

    <script async src="<%= BASE_URL %>script.js"></script>
  </head>

</html>
```

Use the angular-element in your Vue.js project components.

```html
<template>
  <!-- existing template -->

  <angular-element></angular-element>
</template>
```
