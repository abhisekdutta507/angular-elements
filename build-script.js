const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files = [
    './dist/angular-elements/main.js',
    './dist/angular-elements/polyfills.js',
    './dist/angular-elements/runtime.js'
  ]

  await fs.ensureDir('dist/angular-elements')

  await concat(files, 'dist/angular-elements/script.js')
})()